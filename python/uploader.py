#!/Python27/python.exe -u 
#!/usr/bin/env python
# downloaded from https://code.google.com/p/file-uploader/
import cgi, os
import cgitb; cgitb.enable()
import hashlib
import datetime
from pdf2txt import Pdf2Txt


#   Copyright (C) Stephen Reese http://www.rsreese.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.


try: # Windows needs stdio set for binary mode.
    import msvcrt
    import uuid
    msvcrt.setmode (0, os.O_BINARY) # stdin  = 0
    msvcrt.setmode (1, os.O_BINARY) # stdout = 1
except ImportError:
    pass

form = cgi.FieldStorage()

# Generator to buffer file chunks
def fbuffer(f, chunk_size=10000):
   while True:
      chunk = f.read(chunk_size)
      if not chunk: break
      yield chunk

# A nested FieldStorage instance holds the file
fileitem = form['file']

# Test if the file was uploaded
if fileitem.filename:

   # strip leading path from file name to avoid directory traversal attacks
   fn = os.path.basename(fileitem.filename)
  
   # Internet Explorer will attempt to provide full path for filename fix
   fn = fn.split('\\')[-1]
 
   # This path must be writable by the web server in order to upload the file.
   path = "E:\\lengiotempfolder\\"
   filepath = path + fn

   # Open the file for writing 
   f = open(filepath , 'wb', 10000)

   h = hashlib.sha256()
   datalength = 0

   # Read the file in chunks
   for chunk in fbuffer(fileitem.file):
      f.write(chunk)
      h.update(chunk)
      datalength += len(chunk)
   hexdigest = h.hexdigest()
   f.close()

   # Include date in filename, increment version and append hash value
   count = 0
   tmp_fn = filepath + "_" + datetime.datetime.now().strftime("%Y-%m-%d") + ".v" + str(count) + "_" + hexdigest
   while os.path.isfile(tmp_fn):
      count += 1
      tmp_fn = filepath + "_" + datetime.datetime.now().strftime("%Y-%m-%d") + ".v" + str(count) + "_" + hexdigest

   # Rename the file
   os.rename(filepath, tmp_fn)

   # Get the new filename for the users viewing pleasure
   displaypath, tmp_split = os.path.split(tmp_fn)

   
   # ini> added by jhon roa

   convertedText = ""
   error = ""

   # get the pdf cntent and put its content into a .xt file
   Pdf2Txt.run(["pdf2txt.py" , "-o", "E:\\lengiotempfolder\\test.txt" , tmp_fn])   
   # iterates the .txt file
   try:
      lines = open("E:\\lengiotempfolder\\test.txt", 'r').readlines()
      for line in lines:
        convertedText = convertedText + "\n" + line
   except Exception, e:
      error = e

   message = convertedText

   # end> added by jhon roa

else:
   message = 'No file was uploaded'

# return the pdf file text inside a html page
print """\
Content-Type: text/html\n
  <html>
    <head>
      <script type="text/javascript" src="../FileSaver.js"></script>
      <script type="text/javascript" src="../jquery-1.10.2.js"></script>   
      <script type="text/javascript">

        // creates a text file to download given a text variable
        function downloadFile(sourceText){
          var blob = new Blob([sourceText], {type: "text/plain;charset=utf-8"});
          saveAs(blob, "content.txt");
        }

        // fires whn the downloa button is clicked
        function downloadButtonEvent(){
          var sourceText = $("#pdfTextTextArea").val();
          downloadFile(sourceText);
        }

        // call the previous page
        function backButtonEvent(){
          parent.history.back();
          return false;
        }    
              
      </script>
    </head>
    <body>
      <textarea id="pdfTextTextArea" rows="4" cols="50" style="width:1000px; height:500px;">
        %s 
      </textarea>    
      <input id="downloadButton" type="button" value="Download" onClick="downloadButtonEvent();"> 
      <input id="backButton" type="button" value="Back" onClick="backButtonEvent();"> 
    </body>
  </html>
""" % (message,)

