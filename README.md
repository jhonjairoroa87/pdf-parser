This is a python pdf converter. You will be able to upload a pdf file and extract its text.
Was written in python, html ans javascript.


The content of the "html-js" folder must be placed in the apache htdocs folder, ie:

- htdocs
    - FileSaver.js
	- jquery-1.10.2.js
	- form.htm

The content of the "python" folder must be placed in the apache cgi-bin folder like:

- cgi-bin
    - pdf2txt.py
	- uploader.py
	
Look in the httpd.config file :
	
 - Verify that the apache cgi is enabled, ie:

LoadModule cgi_module modules/mod_cgi.so

 - Verify that the apache cgi supports the python document handling, ie

AddHandler cgi-script .cgi .py

 - also verify the cgi directory permissions, ie
 
 <Directory "C:/Program Files (x86)/Apache Software Foundation/Apache2.2/cgi-bin">
    Options Indexes FollowSymLinks Includes ExecCGI
    AllowOverride All
    Order deny,allow
    Allow from all
</Directory>	

Restart Apache

install pdfminer:
http://www.unixuser.org/~euske/python/pdfminer/index.html